const logger = require("loglevel");
const stripHtml = require("string-strip-html");

global.Olm = require('olm');
const sdk = require("matrix-js-sdk");

//tweak to your own
const creds = require("./creds.json");
const clientOptions = creds;

const { LocalStorage } = require('node-localstorage');
const localStorage = new LocalStorage('./local_storage/');
const LocalStorageCryptoStore = require('./node_modules/matrix-js-sdk/lib/crypto/store/localStorage-crypto-store');

// yargs:
const botParser = require('yargs');

const settingsCommand =  require('./modules/settingscommand.js');
const { WebhookCommandStore } = require('./modules/webhookcommand.js');
const webhookCommandStore = new WebhookCommandStore(localStorage);

const { HttpClient } = require('./modules/httpclient.js');
const httpClient = new HttpClient();

const MarkdownIt = require('markdown-it'),
  md = new MarkdownIt();
client = null;

const WEBHOOKER = {
  'FORBIDDEN_COMMAND' : 'Not allowed to run this command, ask to moderator or administrator.'
};

async function start() {
    logger.setLevel("debug");
    if (creds.jwtSecret == "hackme")  throw new Error("Change jwtSecret in credentials file, please");
    if (!localStorage.getItem('accessToken')) {
      logger.debug("No stored access token found.");
      loginClient = sdk.createClient(clientOptions);
      loginClient.login("m.login.password", {"user": creds.userId, "password": creds.password, initial_device_display_name: 'Matrix webhooker'}).then((response) => {
        logger.debug("Successful login with credentials from creds.json.");
	localStorage.setItem('baseUrl', creds.baseUrl);
	localStorage.setItem('accessToken', response.access_token);
	localStorage.setItem('userId', response.user_id);
	localStorage.setItem('deviceId', response.device_id);
	localStorage.setItem('jwtSecret', creds.jwtSecret);
	loginClient.stopClient();
	startWithAccessToken();
      });
    } else {
      logger.debug("Stored access token found.");
      startWithAccessToken();
    }
}

async function startWithAccessToken() {
  clientOptions.accessToken = localStorage.getItem('accessToken');
  clientOptions.userId = localStorage.getItem('userId');
  clientOptions.deviceId = localStorage.getItem('deviceId');

  // STORES:
  clientOptions.sessionStore = new sdk.WebStorageSessionStore(localStorage);
  clientOptions.store = new sdk.MemoryStore();

  // To make the e2ee work we need to store some keys.

  // If using localstorage cryptostore
  logger.debug("Using localstorage to store crypto");
  clientOptions.cryptoStore = new LocalStorageCryptoStore.LocalStorageCryptoStore(localStorage);
  clientOptions.cryptoStore.startup();

  client = sdk.createClient(clientOptions);

  await client.initCrypto()
  
  // Enable the features to be used
  // autoAcceptInvites(client); 
  //
  client.startClient({ initialSyncLimit: 0 });
  const checkSyncState = (state, prevState, data) => {
    switch (state) {
      case 'PREPARED':
	logger.debug("Successful login with credentials from local storage.");
	setupDefaultOptions();
	addCommands();
	client.getRooms();
	client.uploadKeys(); 
	setupBindings();
	client.removeListener('sync', checkSyncState);
	break;
      case 'ERROR':
	client.removeListener('sync', checkSyncState);
	break;
      default:
    }
  };
  client.on('sync', checkSyncState);
}

/**
 * Set the default options in case they aren't set.
 */
function setupDefaultOptions(){
  if (!localStorage.getItem('autoverify_unknown')) {
    localStorage.setItem('autoverify_unknown', true);
  }
}

/**
 * Add bot commands.
 */
function addCommands() {
  console.log("SET bot_commander");

  botParser
    .scriptName("!webhooker")
    .commandDir('commands')
    .strict()
    .help()
    .epilog("Webhooker Matrix-bot");
}

function setupBindings() {
  logger.debug("Setting Bindings");
  client.on("event", async function ( event ) {
    //TRACING only
    //logger.trace(event);
  });
  client.on("Room.timeline", async function (event, room, toStartOfTimeline) {
    if (event.isEncrypted()) {
      // handling in handleEventDecrypted
      return;
    }
      // we know we only want to respond to messages
      if (event.getType() !== "m.room.message") {
	  return;
      } else {
	handleMessage(event); 
      }
  });

 client.on("room.message", async function (event, room, toStartOfTimeline) {
    if (event.isEncrypted()) {
      // handling in handleEventDecrypted
      return;
    }
      // we know we only want to respond to messages
      if (event.getType() !== "m.room.message") {
	  return;
      } else {
	handleMessage(event); 
      }
  });

  client.on("Event.decrypted", async function (event){
    logger.debug("Event decryption try of type " +  event.getType())
    if (event.isDecryptionFailure()) {
      logger.error("Decryption failure: " + event);
      return;
    }
    if (event.getType() === "m.room.message"){
      handleMessage(event);
    }
  });

  client.on("RoomMember.membership", async function (event, member) {
    let mdirect_events = client.getAccountData('m.direct');
    const direct_events_content = mdirect_events ? mdirect_events.getContent() : {};

    // User leaves the DM rom, Need to remove from bot accountdata as m.direct
    // As someone in DM doesn't matter who is, the direct message chat is silly with one
    if (member.membership === "leave"){ //&& direct_events_content[member.userId]){
      // If it can avoid the for because the user leaving
      // is not the bot it's better and faster going to the key in array directly
      if (direct_events_content[member.userId]){
        const fast_dm_room_index = direct_events_content[member.userId].indexOf(event.getRoomId())
	if (fast_dm_room_index > -1) {
          mdirect_events.event.content[member.userId].splice(fast_dm_room_index, 1);
	  client.setAccountData('m.direct', mdirect_events.getContent());
	  return;
	}
      }
      // Otherwise lookup for the room in all the m.direct conversations in accountdata
      for (const user_id in direct_events_content){
	const dm_room_index = direct_events_content[user_id].indexOf(event.getRoomId());
	if (dm_room_index > -1) {
	  // Remove from Account Data as DM if it is
	  mdirect_events.event.content[user_id].splice(dm_room_index, 1);
	  client.setAccountData('m.direct', mdirect_events.getContent());
	  return;
	}
      }
    }

    // User invites the bot, Need to auto-join and add to bot accountdata as m.direct
    // member.userId is the invited user, and here we want to be the bot
    if (member.membership === "invite" && member.userId === client.getUserId()) {
      //TODO By now only invitations from users from same server are allowed 
      //when safeness assertion done it must be changed 
      if (!event.getSender().endsWith(client.getUserId().substr(client.getUserId().lastIndexOf(":"))))
        return
      client.joinRoom(member.roomId).then((room) => {
        logger.debug("Auto-joined %s", room.roomId);
        const peerUser = event.getSender();
        // Mark in Account Data as DM if it is
        addDMAccountData(event, mdirect_events, direct_events_content, peerUser)
      });
    }

    // Bot invites User , Need to add to bot accountdata as m.direct if is_direct
    // Here we look for invitations which are sent by the bot
    if (member.membership === "invite" && event.getSender() === client.getUserId()){
      const peerUser = member.userId;
      addDMAccountData(event, mdirect_events, direct_events_content, peerUser)
    }
  });

}

/**
 *
 * Adds Direct Message account data from an invitation event.
 *
 * @param {MatrixEvent} event - the event of the invitation.
 * @param {Object} mdirect_events - The existing map of m.direct.
 * @param {Object} direct_events_content - The existing m.direct events content
 * @param {string} peerUser - The user id who is inviting or we invite in a direct message
 *
 */
function addDMAccountData(event, mdirect_events, direct_events_content, peerUser){
  if (event.getContent().is_direct && !direct_events_content[peerUser]){
    if (mdirect_events){
      //TODO if undefined when new user  it fails 
      mdirect_events.event.content[peerUser] = [];
    } else {
      var initDmMap = {
        content: {
          [peerUser]: [
          ]
        }
      };
    }
  }
  if (event.getContent().is_direct && (!direct_events_content[peerUser] || direct_events_content[peerUser].indexOf(event.getRoomId()) == -1)){
    debugger
    let fixDmMap = mdirect_events? mdirect_events.event : initDmMap
    fixDmMap.content[peerUser].push(event.getRoomId())
    client.setAccountData('m.direct', fixDmMap.content);
  }
}

/**
 *
 * Verify user's device. Basically it means that the bot can trust remote user's e2e keys.
 *
 * @param {string} userId - user id.
 * @param {string} deviceId - device body.
 */
async function verifyDevice(userId, deviceId) {
  if (!userId || typeof userId !== 'string') {
    throw new Error('"userId" is required and must be a string.');
  }
  if (!deviceId || typeof deviceId !== 'string') {
    throw new Error('"deviceId" is required and must be a string.');
  }
  await client.setDeviceKnown(userId, deviceId, true);
  await client.setDeviceVerified(userId, deviceId, true);
}


async function handleMessage (event){
  // Command about custom webhooks administration
  if ( event.getContent().body.startsWith('!webhooker')){
    if (canRunCommand(event.getRoomId(), event.getSender())) {
      // Remove the webhooker bot trigger part and treat each webhooker command as main script
      const args = event.getContent().body.substring("!webhooker".length)

      const context = {}
      botParser.parse(args || '', context, (err, argv, output) => {
	console.log("Check if could be parsed");
	if (err) {
	  logger.error("Bad-Bad")
	  logger.error(err.message)
	  return;
	}
	if (argv.help){
	  // override and format as it may start with !webhooker and enter in a loopback
	  output = md.render("**Webhooker** help:\n ```" + output + "```")
	}
	else {
	  if (argv._ == 'autoverify_unknown'){
	    localStorage.setItem('autoverify_unknown', argv.autoverify_unknown);
	    output = localStorage.setItem("autoverify unknown devices set to " + argv.autoverify_unknown);
	  }
	  if (argv._ == 'command:set'){
	    logger.debug("Add command to sqlite in this room scope");
	    webhookCommandStore.add(event.getRoomId(), argv.custom_command, argv.url,argv.context).then(
	      (result) => {
		let answer = md.render("Succesful set command, summary:\n ```json\n" + JSON.stringify(result, null, 2) + "\n```\nUse this **Token** in counterpart:\n```json\n" + result.token + "\n```");
		sendDirectNotice(event.getSender(), event.getRoomId(), answer);
		sendNotice(event.getRoomId(), "Command set!");
	      }
	    );
	  }
	  if (argv._ == 'command:ls'){
	    logger.log("List command in sqlite in this room scope");
	    logger.log("commandStore.list(room_id);");
	    webhookCommandStore.list(event.getRoomId()).then(
	      (rows, reject) => {
		const commands_list = [];
		const commands_list_text = [];
		const commands_list_admin = [];
		const commands_list_admin_text = [];
		if(reject) return reject;
		rows.forEach(function (row) {
		  commands_list.push(`|${row.custom_command}|${row.url}|${row.context}|`);
		  commands_list_text.push(`custom_command:\t${row.custom_command}\nurl:\t${row.url}\ncontext:${row.context}\n`);
		  commands_list_admin.push(`|${row.custom_command}|${row.url}|${row.context}|${row.token}|`);
		  commands_list_admin_text.push(`custom_command:\t${row.custom_command}\nurl:\t${row.url}\ncontext:\t${row.context}\ntoken:\t${row.token}\n`);
		});
		const answer = md.render("This is the list of available commands in this room:\n" +
		  "| custom_command | url | context |\n |---|---|---|\n"+
		  commands_list.join("\n") + "\n");
		const answer_text = "This is the list of available commands in this room:\n" +
		  commands_list_text.join("------------------------------\n") + "\n";
		const answer_admin = md.render("This is the list of available commands in this room with tokens:\n" +
		  "| custom_command | url | context | token |\n |---|---|---|---|\n"+
		  commands_list_admin.join("\n") + "\n");
		const answer_admin_text = "This is the list of available commands in this room with tokens:\n" +
	          commands_list_admin_text.join("------------------------------\n") + "\n";
		sendNotice(event.getRoomId(), answer, answer_text);
		sendDirectNotice(event.getSender(), event.getRoomId(), answer_admin, answer_admin_text);
	      }
	    );
	  }
	  if (argv._ == 'command:rm'){
	    logger.debug("Remove command in sqlite in this room scope");
	    webhookCommandStore.remove(event.getRoomId(),argv.command).then((rows_changes)=>{
	      if (rows_changes > 0 ){
		sendNotice(event.getRoomId(), "Command " + argv.command + " succesfully removed");
	      } else {
		sendNotice(event.getRoomId(), "Command doesn't exist");
	      }
	    });
	  }
	}
	if (output) {
	  sendNotice(event.getRoomId(), output.replace(/(?:\r\n|\r|\n)/g, '<br>'))
	}
      }
      );

      if (event.getContent().body.startsWith('!webhooker_reset')) {
	resetDeviceList(event.getRoomId());
      }

    } else {
      // Here the commands usable only by low power level members ( No one else).
      // Quick way to allow low power level members to list the custom commands in the room without going to
      // yargs to prevent leaks.
      if ( event.getContent().body.startsWith('!webhooker command:ls')) {
	logger.log("List command in sqlite in this room scope");
	webhookCommandStore.list(event.getRoomId()).then(
	  (rows, reject) => {
	    const commands_list = [];
	    const commands_list_text = [];
	    if(reject) return reject;
	    rows.forEach(function (row) {
	      commands_list.push( `|${row.custom_command}|${row.url}|${row.context}|`);
	      commands_list_text.push( `custom_command:\t${row.custom_command}\nurl:\t${row.url}\ncontext:${row.context}\n`);
	    });
	    const answer = md.render("This is the list of available commands in this room:\n" +
	      "| custom_command | url | context |\n |---|---|---|\n"+
	      commands_list.join("\n") + "\n");
	    const answer_text = "This is the list of available commands in this room:\n" +
	      commands_list_text.join("------------------------------\n") + "\n";
	    sendNotice(event.getRoomId(), answer, answer_text);
	  }
	);
      }
    }
    return;
  }
  // Custom command runner
  if ( event.getContent().body.startsWith('!') || event.getContent().body.match(/(^\s*(!\S*)+)|(\\n(\s)*(!\S*)+)/m) ){
    const lookup_command = event.getContent().body.match(/(^\s*(!\S*)+)|(\\n(\s)*(!\S*)+)/m)[2]||event.getContent().body.match(/(^\s*(!\S*)+)|(\\n(\s)*(!\S*)+)/m)[5];
    webhookCommandStore.commandLookup(event.getRoomId(),lookup_command).then((custom_command)=>{
      if (custom_command) {
        logger.debug(lookup_command + " is in commandStore let's run it.");
        const result = httpClient.commandRequest(lookup_command, custom_command.url, custom_command.context, custom_command.token, event).then((response) => {
          // Expecting application/json
          if (!response.headers['content-type'].startsWith('application/json') && 
	    !response.headers['content-type'].startsWith('application/vnd.api+json')
	  ){
            logger.log("Expecting a json response, while it is" + response.headers['content-type']);
            sendNotice(event.getRoomId(), "Could not parse the answer from external web. Invalid content-type");
            return;
          }
          // We are looking for data.text to be sent to the room and for the existance of custom_command.token ...
          // A little silly but someday will be better... TODO
          if ((response.headers['authorization']|| "").substring(7) != custom_command.token){
            sendNotice(event.getRoomId(), "Could not parse the answer from external web. Unauthorized response.");
	    return;
	  }
          logger.debug(response.status);
          sendNotice(event.getRoomId(), (response.data || { text : response.status.toString() } ).text );
        })
          .catch((error)=> {
            logger.error(error);
            const answer = md.render("**" + error.response.status + "** : " + error.response.statusText +
              ": Could not parse the answer from external web.");
            sendNotice(event.getRoomId(), answer);
          });
      }
    });
  }
}

/**
 *
 * Check if user has enough power level to run moderator command in specific room.
 *
 * @param {string} room_id - room id.
 * @param {string} user_id - user id.
 *
 * @return {bool}  about user is allowed or not
 */
function canRunCommand(room_id, user_id) {
    const room = client.getRoom(room_id);
    // obtain room member as sender fails to get correct powerLevel
    const room_member = room.getMember(user_id);
    // Get the current state of the room ('f'(forward)) is to get state in newest event
    // of timeline.
    // const room_state = room.getLiveTimeline().getState("f"); // TODO ¿ why not ?
    room_state = room.currentState;

    // Only users of power level > 50 can use this command.
    //
    // This way we can check at current room state if user has sufficient power level
    // to redact events other than their own (moderate or administrate : power level > 50)
    //if (room_state.maySendStateEvent("m.room.bot.options", event.sender.userId)){ // TODO the state event power_levels needs to be present in this room https://matrix-org.github.io/matrix-js-sdk/0.6.1/models_room-state.js.html#line117  also https://github.com/matrix-org/matrix-react-sdk/blob/9aff2e836e5d2459e28fdb4af823be4f7441b3f0/src/ScalarMessaging.js#L418
    if (room_state._hasSufficientPowerLevelFor("redact", room_member.powerLevel)) {
      return true ;
    } else {
      sendNotice(room_id, WEBHOOKER.COMMAND_FORBIDDEN);
      return false;
    }
}

/**
 * Resets the room verified device list of the bot.
 *
 * @param {string} room_id - room id.
 */
async function resetDeviceList(room_id){
  logger.log("Resetting device list knowns and verifieds");
  const room = client.getRoom(room_id);
  const members = (await room.getEncryptionTargetMembers()).map(x => x["userId"])
  const memberkeys = await client.downloadKeys(members);
  for (const userId in memberkeys) {
    for (const deviceId in memberkeys[userId]) {
      await client.setDeviceKnown(userId, deviceId, false);
      await client.setDeviceVerified(userId, deviceId, false);
    }
  }
}

/**
 * Mark all devices in a room as verified.
 *
 * @param {string} room_id - room id.
 */
async function autoVerify(room_id){
  logger.debug("AutoVerify members in room");
  let room = client.getRoom(room_id);
  const e2eMembers = await room.getEncryptionTargetMembers();
  for (const member of e2eMembers) {
    const devices = client.getStoredDevicesForUser(member.userId);
    for (const device of devices) {
      if (device.isUnverified()){
	logger.debug(member.userId + " : " + device.deviceId + " needs autoverification ");
	await verifyDevice(member.userId,device.deviceId)
      }
    }
  }
}

/**
 * Sends a command to user id to perform some private administrative task.
 *
 * @param {string} user_id
 *   The user id that is performing some administrative task
 * @param {string} room_id
 *   The room id for which this administrative task is performed
 * @param {string} body
 *   The body of the message wishing to be sent.
 * @param {string} body_text
 *   The body of the message wishing to be sent in plain text.
 */
async function sendDirectNotice(user_id, room_id, body, body_text){
  // find invited direct message rooms
  // Would be nice to restrict for the ones having m.direct from account data
  const mdirect_events = client.getAccountData('m.direct');
  const direct_events_content = mdirect_events ? mdirect_events.getContent() : {};
  const dms_with_user = direct_events_content[user_id] || [];

  // if not via m-direct filtering of above traverse all rooms where we are...
  //let rooms = client.getRooms();

  const invited_dm_rooms = dms_with_user.filter((room) => {
    const loaded_room = client.getRoom(room);
    if (!loaded_room) return false;
    // We are strict here, if the room is not with just two users it won't be accepted as a safe
    // way to send tokens for commands set.
    return loaded_room.getMyMembership() === 'invite' || loaded_room.getMyMembership() === 'join'
      && loaded_room.getMember(user_id) && loaded_room.getMember(client.getUserId())
      && loaded_room.getInvitedAndJoinedMemberCount() == 2
  });
  if (invited_dm_rooms[0]) {
    sendNotice(invited_dm_rooms[0], body, body_text)
  } else {
    client.createRoom({
      preset: 'trusted_private_chat',
      invite: [ user_id ],
      is_direct: true
    }).then((created_room_id) => {
      sendNotice(room_id, user_id + " : you must start a direct message chat with " + client.getUserId() + " before. Too many eyes here... I cannot send the token safely.\nAccept the invitation or start a new direct message room with " + client.getUserId() + " and resend this command again.")
    })
  }
}

/**
 * Sends a command to room.
 *
 * @param {string} room_id
 *   The room id for which this administrative task is performed
 * @param {string} body
 *   The body of the message wishing to be sent.
 * @param {string} body_text
 *   The body of the message wishing to be sent in plain text.
 */
async function sendNotice(room_id, body, body_text) {
  var content = {
    "body" : body_text || stripHtml(body),
    "format": "org.matrix.custom.html",
    "formatted_body" : body,
    "msgtype" : "m.notice"
    };

  need_verify_all = false;
  logger.debug("Sending " + body + " to " + room_id);
  await client.sendMessage(room_id, content).catch((error) => {
    logger.error("Something wrong, often due device unverified as I am a bot... mark all as verified... i am sorry it's my unsecure condition that involves you..."); 
    // delegate to settings flag autoverify_unknown to decide if needs verification, 
    // as obviously it needs to go ahead. otherwise need_verify_all remains undefined.
    need_verify_all = JSON.parse(localStorage.getItem('autoverify_unknown'));
  })
  // proceed if needs to VERIFY ALL and  also if it's marked as it can't be done in function parameter
  if (need_verify_all === true ){
    await autoVerify(room_id).then(() => { 
      client.sendMessage(room_id, content).catch((error) => {
        logger.error("Something wrong again, often due device unverified as I am a bot. After failing again ther will be no more tries, check logs."); 
      });

    }).catch((error) => {console.log(error)});
  }
}

start();
