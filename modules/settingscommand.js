// Set settings of webhooker

exports.setAutoverifyUnknown = function(option, localStorage){
  // Set bot security in localstorage
  if (option === "autoverify_unknown false"){
    localStorage.setItem('autoverify_unknown', false);
    return "webhooker commands now will run without autoverification";
  }
  else if (option === "autoverify_unknown true"){
    localStorage.setItem('autoverify_unknown', true);
    return "webhooker commands now will run with autoverification";
  }
  else {
    return "Webhooker bot will default to autoverify, so it can send messages " +
    "marking as verified each device of each member.<br/>" +
    "<strong>!webhooker set autoverify_unknown false</strong> will disable this autoverification.<br/>" +
    "<strong>!webhooker set autoverify_unknown true</strong> will enable this autoverification.<br/>" +
    "<strong>!webhooker set autoverify_unknown help</strong> will show this help text." ;
  }
}
