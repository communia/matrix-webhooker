// Http client module
"use strict";

const axios = require("axios");
const logger = require("loglevel");

class HttpClient {
  /**
   * Performs request from a bot custom_command
   * 
   * @param {string} room_id
   *   The room id for which this command will be enabled
   * @param {string} command
   *   The command that will trigger the call to webhook
   * @param {string} url
   *   The endpoint where the content will be sent
   * @param {context} object
   *   Custom context that will be used at user needs
   *
   * @return {?Object}
   *   Response of the request if everything is ok.
   */
  async commandRequest(command, url, context, token, event){
    try {
      const config = {
	headers: { 
	  Authorization: `Bearer ${token}`,
	  Accept: 'application/json',
	  'Content-Type': 'application/json'
	}
      };
      const body = {
	event: event,
	context: context
      }
      const response = await axios.post(url, body, config);
      logger.debug("Http client request worked!:")
      logger.debug(body);
      return await response;
    } catch (error) {
      logger.error(error);
      //return error;
      throw error;
    }

    
  }
}

module.exports = { HttpClient };

