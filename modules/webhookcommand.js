// Set settings of webhooker
"use strict";

const logger = require("loglevel");
const sqlite3 = require("sqlite3");
const jwt = require('jsonwebtoken');

/**
 * Class to store and retrieve custom commands, one can
 * list, lookup, create and remove commands.
 */
class WebhookCommandStore {
  /**
   * @constructor
   *
   * @param {LocalStorage} local_storage 
   *   is needed because  we must get the secret to create a jwt 
   *   token to ensure the wbhook endpoint.
   */
  constructor (local_storage) {
    this.dbFilePath = "./local_storage/webhooks.db"
    this.db = new sqlite3.Database(this.dbFilePath, (err) => {
      if (err) {
	console.log('Could not connect to database', err)
      } else {
	console.log('Connected to database')
      }
    });
    this.localStorage = local_storage;
    this.run('CREATE TABLE IF NOT EXISTS commands (room_id,custom_command,url,context,token, CONSTRAINT custom_command_id PRIMARY KEY (room_id, custom_command));')
  }

  run(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.run(sql, params, function (err) {
	if (err) {
	  console.log('Error running sql ' + sql)
	  console.log(err)
	  reject(err)
	} else {
	  resolve(this.changes)
	}
      })
    })
  }

  get(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.get(sql, params, (err, result) => {
	if (err) {
	  console.log('Error running sql: ' + sql)
	  console.log(err)
	  reject(err)
	} else {
	  resolve(result)
	}
      })
    })
  }

  all(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (err, rows) => {
	if (err) {
	  console.log('Error running sql: ' + sql)
	  console.log(err)
	  reject(err)
	} else {
	  resolve(rows)
	}
      })
    })
  }

  /**
   * Adds a command to commanstore
   *
   * @param {string} room_id
   *   The room id for which this command will be enabled
   * @param {string} custom_command
   *   The command that will trigger the hang to webhook
   * @param {string} url
   *   The endpoint where the content will be sent
   * @param {context} object
   *   Custom context that will be used at user needs
   *
   * @return {Promise} 
   *   With object with columns if everything is ok.
   */
  add(room_id, custom_command, url, context){
    let token = jwt.sign({
      "room_id": room_id,
      "custom_command": custom_command,
      "url": url,
    }, this.localStorage.getItem('jwtSecret'));
    const stringified_context = JSON.stringify(context);
    const sql = `INSERT OR REPLACE into commands (room_id,custom_command,url,context,token) VALUES('${room_id}', '${custom_command}', '${url}', '${stringified_context}', '${token}')`
    const params = []
    return new Promise((resolve, reject) => {
      this.db.run(sql, params, function (err) {
	if (err) {
	  console.log('Error running sql ' + sql)
	  console.log(err)
	  reject(err)
	} else {
	  resolve({
	    room_id: room_id,
	    custom_command: custom_command,
	    url: url,
	    token: token
	  });
	}
      })
    })
  }

  /**
   * Lists the commands in a room
   *
   * @param {string} room_id
   *   The room id for which the list is requested
   * @return {string}
   *   The list of commands
   */
  list(room_id){
    return this.all("SELECT * FROM commands WHERE room_id = ?", [room_id]);
  }

  /**
   * Lists the commands in a room
   *
   * @param {string} room_id
   *   The room id for which the list is requested
   * @return {Promise}
   *   The command or empty
   */
  commandLookup(room_id,command){
    return this.get("SELECT * FROM commands WHERE room_id = ? AND custom_command = ?",
      [room_id, command]
    )
  }

  /** 
   * Lists the commands in a room
   *
   * @param {string} room_id
   *   The room id for which the removal is requested
   * @return {Promise}
   *   The rows affected by the removal
   */
  remove(room_id, command) {
    return this.run(
      `DELETE FROM commands WHERE room_id = ? AND custom_command = ?`,
      [room_id, command]
    )
  }
}

module.exports = { WebhookCommandStore };
