# Webhooker

This bot will allow to room administrators and moderators to generate room custom commands (in a similar way how telegram commands are thought).

When they are invoked will post the message event object data and a predefined context object along with a token to a predefined url: 

```
Authorization: Bearer TTTTOOOKKKEENNNXXXXXXXXX
Content-type: application/json;charset=UTF-8;
{
  "text" : {
    "event" : { The MATRIX MESSAGE EVENT IN JSON }
    "context" : { The constant context predefined when creating the custom command }
  }
}
  
```

If the url where it posts returns a JSON with the same token and a text:

```
{
  token: xxxx ,
  text: "Lorem Ipsum",
}
```

Then the text will get print as room message.


## Commands

It will store commands for each room with these properties:

- room_id: The scope that it will be active
- custom_command: The command that will trigger the posting data to webhook.
- url: The webhook endpoint.
- context: Any context data that will be useful for the webhook to be processed.
- token: The key that will be posted to the webhook to ensure it is legitime.

It depends on the webhook end to verify the token and to process the context.

## Commands:

```
Webhooker help: 
Commands:
!webhooker autoverify_unknown   To set a autoverify the unkown users in a room, true|false
!webhooker command:ls           To get list of defined commands in a room
!webhooker command:rm 		The command to remove in a room, to view the list run command:ls in a room
!webhooker command:set          To set a command in a room 
```

### command:set

To set a command (e.g. `muntilo`) in a room:

`!webhooker command:set !muntilo https://webhook.url/somewhere {"context": "in json"}`

This command can only be executed by a moderator(those who can redact[edit] messages) in the room. It will need a direct message chat with the creator of the command to receive the token (and all the info added in commands store). The bot will try to create this chat if not exists.

### command:ls

To obtain the list of defined commands in this room:

`!webhooker command:ls`

This command has two ways if executed by lower power level member in a room then it will return a list of custom commands in the room, if is executed by a moderator (those who can redact[edit] messages) then it will return the same and additionally the same list but with tokens in the direct message chat.

### command:rm

To remove command `!muntilo` in a room:

`!webhooker command:rm !muntilo`

This command can only be executed by a moderator(those who can redact[edit] messages) in the room.

### autoverify_unknown

To set a autoverify the unkown users in a room, true|false:

`!webhooker autoverify_unknown`

If must autoverify all devices in the room or not.

#### Notes about bot in a e2ee rooms.

`!webhooker autoverify_unknown true` or `!webhooker autoverify_unknown false` The bot can be
configured to be strict, or to be less strict with verification. 

To be a secure room, the bot (its administrator) must verify other member devices
and other members must verify bot devices (Of course own devices also must be
verified).

Bots cannot do human verifying, what it means is that in a e2ee room the base 
is to verify each device per user, as it is a bot it cannot proceed to verify 
or be verified as a human, so it must be verified by a user that administrates it 
behind.

So we need two things to get full security:

##### Verify the bot by members:

Bob creates the user that will be the bot and invites it in an encrypted room. 
Bob Login to riot using bot credentials and verifies the bot service device(where 
the code of this project is executed). It will help to allow verification later as
 using bot in a riot device can be used to crossign devices.(own device that is 
trusted by same user can be a device trusted by others).

  Then to be able to be verified, each user using each device, must proceed
  to verify with this procedure only A procedure is implemented:
  ```mermaid
  graph LR
    Alice1(Alice request the bot device verification.);
    Alice1-->Bob1(A - Bob as a bot administrator logins with riot client using the bot credentials and proceed<br/>to verify each other. As is crossigned it will verify both riot and service device)
Alice1-->Bob2(B - The bot sends via DM a verification request to Bob -the bot administrator- and then he<br/>proceeds to verify via adding a thumbsup: reaction.)
  ```

##### Verify members by bot:

The administrator must login using bot credentials with riot (or other client) 
and proceed to verify devices.

##### Members must verify own devices
Every user must be strict with their devices and verify or reject they. As it 
will prevent to sent messages by bot if it is in secure mode.

##### Disable security
One can abandon the security constraints and set the
unsecure flag with: `!webhooker autoverify_unknown true`


##### Default security state
As verifying with DM to bot administrator is not yet implemented, and to make things 
easier the bot will default to autoverify, so it can send messages marking as 
verified each device of each member.

But if you are strict with security you can set the bot to be secure, with:
`!webhooker autoverify_unknown false` then it will not send messages **if there's any
unverified device in a room**. And you must proceed to verify devices manually as 
noted.
