const logger = require("loglevel");

exports.command = 'command:set <custom_command> <url> [context]'
exports.describe = 'To set a command <custom_command> in a room'
exports.builder = function(yargs) {
  yargs
    .positional('custom_command', {
      describe: 'The custom command which is being set.',
      type: 'string'
    })
    .positional('url', {
      describe: 'The url where command will try to contact carrying context and data.',
      type: 'string'
    })
    .option('context',{
      describe: 'Set of properties in form of JSON object to send static context along with concrete data, everytime a webhook is invoked via command, for example the id of a remote group.',
      type: 'string',
      default: '{}'
    })
}
exports.handler = function (argv) {
  //argv.respond("SO YOU WANT TO set a command?? " + argv.strings.join(' '))
  logger.log("Setting a command " + argv.custom_command + " to point to this url " + argv.url + " with this context " + argv.context )
  try{
    debugger
    argv.context = JSON.parse(argv.context);
  } catch(e){
    if (e instanceof SyntaxError) {
    logger.error(e)
    logger.error("Due to userinput malformed context will result in empty object.")
    argv.context = {"error" : "malformed JSON context, check syntax of JSON"}
    } else {
      throw e;
    }
  }
  logger.debug("Carrying this context");
  logger.debug(argv.context);
}
